const CarController = require("./CarController");
const { Car, UserCar } = require("../models");
const dayjs = require("dayjs");
const { Op } = require("sequelize");
const CarAlreadyRentedError = require("../errors/CarAlreadyRentedError");

describe("CarController", () => {
  describe("#handleListCars", () => {
    it("should call res.status(200) and res.json with list of car instances", async () => {
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const query = {
        size: "MEDIUM",
        availableAt: new Date(),
      };

      const mockRequest = { query };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const cars = [];

      for (let i = 0; i < 10; i++) {
        const car = new Car({ name, price, size, image, isCurrentlyRented });
        cars.push(car);
      }

      const mockCarModel = { findAll: jest.fn().mockReturnValue(cars), count: jest.fn().mockReturnValue(10) };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: { userId, carId, rentStartedAt, rentEndedAt },
        dayjs,
      });
      await carController.handleListCars(mockRequest, mockResponse);

      expect(mockCarModel.findAll).toHaveBeenCalled();
      expect(mockResponse.status).toHaveBeenCalledWith(200);
      expect(mockResponse.json).toHaveBeenCalledWith({
        cars,
        meta: {
          pagination: {
            page: 1,
            pageCount: 1,
            pageSize: 10,
            count: 10,
          },
        },
      });
    });
  });

  describe("#handleGetCar", () => {
    it("should call res.status(200) and res.json with car instance", async () => {
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const mockRequest = {
        params: {
          id: 1,
        },
      };

      const mockCar = new Car({ name, price, size, image, isCurrentlyRented });
      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });
      const mockCarModel = {
        findByPk: jest.fn().mockReturnValue(mockCar),
      };
      const mockUserCarModel = { mockUserCar };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const carController = new CarController({ carModel: mockCarModel, userCarModel: mockUserCarModel, dayjs });
      await carController.handleGetCar(mockRequest, mockResponse);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
      expect(mockResponse.status).toHaveBeenCalledWith(200);
      expect(mockResponse.json).toHaveBeenCalledWith(mockCar);
    });
  });

  describe("#handleCreateCar", () => {
    it("should call res.status(201) and res.json with car instance", async () => {
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const mockRequest = {
        body: {
          name,
          price,
          size,
          image,
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockCar = new Car({ name, price, size, image, isCurrentlyRented });
      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });

      const mockCarModel = { create: jest.fn().mockReturnValue(mockCar) };
      const mockUserCarModel = { mockUserCar };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleCreateCar(mockRequest, mockResponse);

      expect(mockCarModel.create).toHaveBeenCalled();
      expect(mockResponse.status).toHaveBeenCalledWith(201);
      expect(mockResponse.json).toHaveBeenCalledWith(mockCar);
    });

    it("should call res.status(422) and res.json with error name and message", async () => {
      const err = new Error("Something Error");

      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const mockRequest = {
        body: {
          name,
          price,
          size,
          image,
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockCarModel = { create: jest.fn().mockReturnValue(Promise.reject(err)) };
      const mockUserCarModel = { userId, carId, rentStartedAt, rentEndedAt };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleCreateCar(mockRequest, mockResponse);

      expect(mockCarModel.create).toHaveBeenCalledWith({ name, price, size, image, isCurrentlyRented });
      expect(mockResponse.status).toHaveBeenCalledWith(422);
      expect(mockResponse.json).toHaveBeenCalledWith({
        error: {
          name: err.name,
          message: err.message,
        },
      });
    });
  });

  describe("#handleRentCar", () => {
    it("should call res.status(201) and res.json with userCar instance", async () => {
      const id = 1;
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const mockRequest = {
        params: {
          id: 1,
        },
        body: {
          rentStartedAt,
          rentEndedAt,
        },
        user: {
          id: 1,
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockNext = jest.fn();

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });
      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });

      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };
      const mockUserCarModel = {
        findOne: jest.fn().mockReturnValue(null),
        create: jest.fn().mockReturnValue({
          userId: mockUserCar.userId,
          carId: mockUserCar.carId,
          rentStartedAt: mockUserCar.rentStartedAt,
          rentEndedAt: mockUserCar.rentEndedAt,
        }),
      };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleRentCar(mockRequest, mockResponse, mockNext);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
      expect(mockUserCarModel.findOne).toHaveBeenCalledWith({
        where: {
          carId: mockCar.id,
          rentStartedAt: {
            [Op.gte]: mockRequest.body.rentStartedAt,
          },
          rentEndedAt: {
            [Op.lte]: mockRequest.body.rentEndedAt,
          },
        },
      });
      expect(mockUserCarModel.create).toHaveBeenCalledWith({
        userId: mockRequest.user.id,
        carId: mockCar.id,
        rentStartedAt: mockRequest.body.rentStartedAt,
        rentEndedAt: mockRequest.body.rentEndedAt,
      });
      expect(mockResponse.status).toHaveBeenCalledWith(201);
      expect(mockResponse.json).toHaveBeenCalledWith({
        userId: mockUserCar.userId,
        carId: mockUserCar.carId,
        rentStartedAt: mockUserCar.rentStartedAt,
        rentEndedAt: mockUserCar.rentEndedAt,
      });
    });

    it("should call the next function with error", async () => {
      const err = new Error("Somthing Error");
      const id = 1;
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const mockRequest = {
        params: {
          id: 1,
        },
        body: {
          rentStartedAt: new Date(),
          rentEndedAt: new Date(),
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockNext = jest.fn();

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });

      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };
      const mockUserCarModel = { findOne: jest.fn(() => Promise.reject(err)) };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleRentCar(mockRequest, mockResponse, mockNext);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
      expect(mockUserCarModel.findOne).toHaveBeenCalledWith({
        where: {
          carId: mockCar.id,
          rentStartedAt: {
            [Op.gte]: mockRequest.body.rentStartedAt,
          },
          rentEndedAt: {
            [Op.lte]: mockRequest.body.rentEndedAt,
          },
        },
      });
      expect(mockNext).toHaveBeenCalled();
    });

    it("should call res.status 422 and res.json with error", async () => {
      const id = 1;
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const mockRequest = {
        params: {
          id: 1,
        },
        body: {
          rentStartedAt: new Date(),
          rentEndedAt: new Date(),
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockNext = jest.fn();

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });
      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });

      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };
      const mockUserCarModel = { findOne: jest.fn().mockReturnValue(mockUserCar) };

      const err = new CarAlreadyRentedError(mockCar);

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleRentCar(mockRequest, mockResponse, mockNext);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
      expect(mockUserCarModel.findOne).toHaveBeenCalledWith({
        where: {
          carId: mockCar.id,
          rentStartedAt: {
            [Op.gte]: mockRequest.body.rentStartedAt,
          },
          rentEndedAt: {
            [Op.lte]: mockRequest.body.rentEndedAt,
          },
        },
      });
      expect(mockResponse.status).toHaveBeenCalledWith(422);
      expect(mockResponse.json).toHaveBeenCalledWith(err);
    });
  });

  describe("#handleUpdateCar", () => {
    it("should call res.status(201) and res.json with car instance", async () => {
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const mockRequest = {
        params: {
          id: 1,
        },
        body: {
          name,
          price,
          size,
          image,
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const mockCar = new Car({ name, price, size, image, isCurrentlyRented });
      mockCar.update = jest.fn().mockReturnThis();
      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });

      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };
      const mockUserCarModel = { mockUserCar };

      const carController = new CarController({
        carModel: mockCarModel,
        userCarModel: mockUserCarModel,
        dayjs,
      });
      await carController.handleUpdateCar(mockRequest, mockResponse);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
      expect(mockCar.update).toHaveBeenCalledWith({ name, price, size, image, isCurrentlyRented });
      expect(mockResponse.status).toHaveBeenCalledWith(200);
      expect(mockResponse.json).toHaveBeenCalledWith(mockCar);
    });

    // it("should call res.status(422) and res.json with error name and message", async () => {
    //   const name = "Avanza Veloz";
    //   const price = 300000;
    //   const size = "MEDIUM";
    //   const image = "https://source.unsplash.com/517x517";

    //   const userId = 1;
    //   const carId = 1;
    //   const rentStartedAt = new Date();
    //   const rentEndedAt = new Date();

    //   const err = new Error("Something Error");

    //   const mockRequest = {
    //     params: {
    //       id: 1,
    //     },
    //     body: {
    //       name,
    //       price,
    //       size,
    //       image,
    //     },
    //   };

    //   const mockResponse = {
    //     status: jest.fn().mockReturnThis(),
    //     json: jest.fn().mockReturnThis(),
    //   };

    //   const mockCarModel = { findByPk: jest.fn(() => Promise.reject(err)) };
    //   const mockUserCarModel = { userId, carId, rentStartedAt, rentEndedAt };

    //   const carController = new CarController({
    //     carModel: mockCarModel,
    //     userCarModel: mockUserCarModel,
    //     dayjs,
    //   });
    //   await carController.handleUpdateCar(mockRequest, mockResponse);

    //   expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
    //   expect(mockResponse.status).toHaveBeenCalledWith(422);
    //   expect(mockResponse.json).toHaveBeenCalledWith({
    //     error: {
    //       name: err.name,
    //       message: err.message,
    //     },
    //   });
    // });
  });

  describe("#handleDeleteCar", () => {
    it("it should be respond with res.status(204) and pseudo delete", async () => {
      const id = 1;
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const mockRequest = {
        params: {
          id: 1,
        },
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        end: jest.fn().mockReturnThis(),
      };

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });
      mockCar.destroy = jest.fn().mockReturnValue(mockCar);

      const carController = new CarController({ carModel: mockCar });
      await carController.handleDeleteCar(mockRequest, mockResponse);

      expect(mockCar.destroy).toHaveBeenCalledWith(1);
      expect(mockResponse.status).toHaveBeenCalledWith(204);
      expect(mockResponse.end).toHaveBeenCalledWith();
    });
  });

  describe("#getCarFromRequest", () => {
    it("should return car", async () => {
      const id = 1;
      const name = "Avanza Veloz";
      const price = 300000;
      const size = "MEDIUM";
      const image = "https://source.unsplash.com/517x517";
      const isCurrentlyRented = false;

      const mockRequest = {
        params: {
          id: 1,
        },
      };

      const mockCar = new Car({ id, name, price, size, image, isCurrentlyRented });
      const mockCarModel = { findByPk: jest.fn().mockReturnValue(mockCar) };

      const carController = new CarController({ carModel: mockCarModel });
      await carController.getCarFromRequest(mockRequest);

      expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
    });
  });

  describe("#getListQueryFromRequest", () => {
    it("should return object of query", async () => {
      const userId = 1;
      const carId = 1;
      const rentStartedAt = new Date();
      const rentEndedAt = new Date();

      const query = {
        size: "MEDIUM",
        availableAt: new Date(),
      };

      const mockRequest = {
        query,
      };

      const mockUserCar = new UserCar({ userId, carId, rentStartedAt, rentEndedAt });
      const mockUserCarModel = { mockUserCar };

      const carController = new CarController({ userCarModel: mockUserCarModel });
      const result = await carController.getListQueryFromRequest(mockRequest);

      const include = {
        model: mockUserCarModel,
        as: "userCar",
        required: false,
        where: {
          rentEndedAt: {
            [Op.gte]: query.availableAt,
          },
        },
      };

      expect(result).toStrictEqual({
        include,
        where: {
          size: query.size,
        },
        limit: 10,
        offset: 0,
      });
    });
  });
});
