const ApplicationController = require("./ApplicationController");
const NotFoundError = require("../errors/NotFoundError");

describe("ApplicationController", () => {
  describe("#handleGetRoot", () => {
    it("should call res.status(200) and res.json with status OK and also message BCR API is up and running!", async () => {
      const status = "OK";
      const message = "BCR API is up and running!";

      const mockRequest = {};

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const application = new ApplicationController();
      await application.handleGetRoot(mockRequest, mockResponse);

      expect(mockResponse.status).toHaveBeenCalledWith(200);
      expect(mockResponse.json).toHaveBeenCalledWith({
        status,
        message,
      });
    });
  });

  describe("#handleNotFound", () => {
    it("should call res.status(404) and res.json with error name, message and details", async () => {
      const mockRequest = {
        method: "GET",
        url: "http://localhost.8000/errornotfound",
      };

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const err = new NotFoundError(mockRequest.method, mockRequest.url);

      const application = new ApplicationController();
      await application.handleNotFound(mockRequest, mockResponse);

      expect(mockResponse.status).toHaveBeenCalledWith(404);
      expect(mockResponse.json).toHaveBeenCalledWith({
        error: {
          name: err.name,
          message: err.message,
          details: err.details,
        },
      });
    });
  });

  describe("#handleError", () => {
    it("should call res.status(500) and res.json with error name, message and details", async () => {
      const err = new Error("Something Error");

      const mockRequest = {};

      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };

      const application = new ApplicationController();
      await application.handleError(err, mockRequest, mockResponse);

      expect(mockResponse.status).toHaveBeenCalledWith(500);
      expect(mockResponse.json).toHaveBeenCalledWith({
        error: {
          name: err.name,
          message: err.message,
          details: err.details || null,
        },
      });
    });
  });

  describe("#getOffsetFromRequest", () => {
    it("should return offset", async () => {
      const query = {
        page: 1,
        pageSize: 10,
      };

      const mockRequest = {
        query,
      };
      const application = new ApplicationController();
      const result = await application.getOffsetFromRequest(mockRequest);

      expect(result).toEqual(0);
    });
  });

  describe("#buildPaginationObject", () => {
    it("should return page, pageCount, pageSize and count", async () => {
      const query = {
        page: 1,
        pageSize: 10,
      };
      const count = 5;
      const mockRequest = {
        query,
      };

      const pageCount = Math.ceil(count / query.pageSize);

      const application = new ApplicationController();
      const result = await application.buildPaginationObject(mockRequest, count);

      expect(result).toStrictEqual({
        page: mockRequest.query.page,
        pageCount,
        pageSize: mockRequest.query.pageSize,
        count,
      });
    });
  });
});
